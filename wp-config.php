<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Q1g5etVO9KI74wa5x8V+Bw/RkvlpqRg1UJtCnGmjpJbUA4LKtI4vC7VM9bfc308JJ8zf/tQ++fs+Eub1RFUI+g==');
define('SECURE_AUTH_KEY',  'dPvkjuSdNMDsVG+cdJwsLQPxTSfiDSPKQGDNbQKWiqphv0WbnOwNdVQQZP6sJJYIKmtQeJydK9C6TEa411hGfQ==');
define('LOGGED_IN_KEY',    'L7Q/WMzkT52XDjAKTC9iuDTcQWGecuvcE/D6mA9dbOqr0Fe/ls1qozTy5B/V7fqLqyBP2E9kxs5yU+Tl7AnZOQ==');
define('NONCE_KEY',        'Anx88ve9sLli+8/NCdS3Rx7oJv5K0x7XmzK1DbOXY/KhtXo/73/qFFZuXsz3BA+ttf+1Tinw4QCRrSyRdWWFKA==');
define('AUTH_SALT',        'Nid8WQAPV2I81uTZfM5Vj8fhOTQt4QHTIiK3gsQZVBmCYeAiT46xyJsZdzKPDgs5K7IGPqbHDey7OQIZoM2OuA==');
define('SECURE_AUTH_SALT', '4vIiBqgjks38fdo08Lv9bvhmDtJhzhJHVbuR0K9pmQ8tynt29jfZenIqvcikvPJ73hh3wyb0pLwJ9ILGTCcxmA==');
define('LOGGED_IN_SALT',   'd/89I3ABuT5cljNq/CUJUec3MMvRBFowgFE25WmbX/H3MUir6VsXdZqKZYkdZXyWac7RT7I5CXOnVMdeg0UaGg==');
define('NONCE_SALT',       'msLAvlTWDNq4EZjkP8mna2JoV39UXXocP1tf8vuZEf+8cUnkoUyodvj6I9fUKkkip3AbZB62cWSAMxRjOY2dew==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
