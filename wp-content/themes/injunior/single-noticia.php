<?php 
    /*
        Template Name: Home Page
    */
?>
<?php get_header()?>
    <main>
        <h1><?php the_title()?></h1>
        <?php 
        // get_post_thumbnail();
        the_content(  );?>
        <?php $args =  array(
    'post_type' => 'noticia',
    'posts_per_page' => 3, 
    'orderby'   => 'rand',
    'post_status' => 'publish',
    'suppress_filters' => false,
);
 

$news = new WP_Query($args);?>
<?php 
if ($news -> have_posts() ) : 
    while ($news ->  have_posts() ) : $news->the_post(); ?>
        <h3><?php the_title(  )?></h3>
        <a href="<?php the_permalink(  )?>">Link</a>
<?php endwhile; else : ?>
     <p><?php esc_html_e( 'Não temos Posts' ); ?></p>
<?php endif; ?>
<?php 
$big = 9999999999999999;
echo paginate_links(array(
    'base'=>str_replace($big,'%#%',esc_url(get_pagenum_link( $big))),
    'format'=>max(1,get_query_var('paged')),
    'current'=>$news->max_num_pages,
    'total'=>'?paged=%#%',
)  );
add_shortcode('wpb-random-posts','wpb_rand_posts');
add_filter('widget_text', 'do_shortcode');
wp_reset_postdata(  );?>
      
    </main>
<?php get_footer()?>

