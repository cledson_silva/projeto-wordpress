<?php 
    /*
        Template Name: Home Page
    */
?>
<?php get_header()?>
    <main>
        <h1><?php the_title(  )?></h1>
        <h2>front-page.php</h2>
        <p><?php the_field('paragrafo')?></p>
        <?php $image = (get_field("imagem"))?>
        <?php console_log( $image);?>
        <img  src="<?php echo $image['url']?>" alt="">
        <!-- Server para pegar partes de includes (blocos/partes de um site) -->
        <?php get_template_part('inc','section',array('key'=>'um texto qualquer'))?>
    </main>
<?php get_footer()?>

