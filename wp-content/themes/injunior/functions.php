<?php 
//  Essa funcão só funcionaria se eu chamasse ela, mas isso aconteria depois da
//  pagina iniciar, por isso é nessesário usar do "add_action()"
//  para que a funcao seja chamada em conjunto com o inicio da pagina
    function psInJunior20212_scripts(){
        wp_enqueue_style('psInJunior2021.2-reset-style', get_template_directory_uri( ) . '/css/reset.css');
        wp_enqueue_style('psInJunior2021.2-style', get_template_directory_uri( ) . '/style.css');
    }add_action('wp_enqueue_scripts','psInJunior20212_scripts');
// Serve para nao ser necessario colocar o titulo da pag
    add_theme_support('title-tag');
    add_theme_support('menus');
    // add_theme_support('thumbnail',array('noticia'));

    // register post type
    function custom_post_type_noticias() {
        register_post_type( 'noticia',array(
            'labels'             => 'Noticias',
            'description'        => 'Descrição Noticias',
            'menu_position'      => 2,
            'public'             => true,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'capability_type'    => 'post',
            'map_meta_cap'       => true,
            'hierarchical'       => false,
            'query_var'          => true,
            'supports'           => array( 'title', 'editor','thumbnail'),
        
        'labels' => array(
            'name'                  => 'Noticia',
            'singular_name'         => 'Noticia',
            'menu_name'             => 'Noticias',
            'add_new'               => 'Nova Noticia',
            'add_new_item'          => 'Adicionar Nova Noticia',
            'edit'                  => 'Editar Noticia',
            'edit_item'             => 'Editar Noticia',
            'new_item'              => 'Nova Noticia',
            'view'                  => 'Ver Noticia',
            'view_item'             => 'Ver Noticia',
            'search_items'          => 'Procurar Noticia',
            'not_found'             =>'Nenhuma Noticia Encontrada',
            'not_found_in_trash'    => 'Nenhuma Notiia Encontrada no Lixo', )
     
        ));}
        add_action( 'init', 'custom_post_type_noticias' );
    //     // adicionando Noticias ao Search
    // function tg_include_custom_post_types_in_search_results( $query ) {
    //     if ( $query->is_main_query() && $query->is_search() && ! is_admin() ) {
    //         $query->set( 'post_type', array( 'post', 'noticia') );
    //     }
    // }
    // add_action( 'pre_get_posts', 'tg_include_custom_post_types_in_search_results' );
    function mySearchFilter($query) {
        if ($query->is_search) {
        $query->set('post_type', 'noticia');
        }
        return $query;
        }add_filter('pre_get_posts','mySearchFilter');
        function console_log( $data ){
            echo '<script>';
            echo 'console.log('. json_encode( $data ) .')';
            echo '</script>';
          }
?>