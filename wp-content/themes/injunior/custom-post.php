<?php 
    /*
        Template Name: Custom Posts
    */
?>
<?php get_header()?>
    <main>
        <h1>Essa aqui é a custom post.php</h1>
    
        <h2>Custom Post:</h2>
       
      <?php
 
// The Query

$args =  array(
    'post_type' => 'noticia',
    'post_status' => 'publish',
    'suppress_filters' => true,
    'orderby' => 'post_date',
    'order' => 'DESC',
);
$news = new WP_Query($args);?>
 
<?php
  get_search_form(  ); 
if ($news -> have_posts() ) : 
    while ($news ->  have_posts() ) : $news->the_post(); ?>
        <h3><?php the_title(  )?></h3>
        <a href="<?php the_permalink(  )?>">Link</a>
<?php endwhile; else : ?>
     <p><?php esc_html_e( 'Não temos Posts' ); ?></p>
<?php endif; 
$big = 9999999999999999;
echo paginate_links(array(
    'base'=>str_replace($big,'%#%',esc_url(get_pagenum_link( $big))),
    'format'=>max(1,get_query_var('paged')),
    'current'=>$news->max_num_pages,
    'total'=>'?paged=%#%',
)  );
wp_reset_postdata(  );?>
    </main>
<?php get_footer()?>

